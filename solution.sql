Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.187 sec)

MariaDB [(none)]> SHOW TABLES;
ERROR 1046 (3D000): No database selected
MariaDB [(none)]>
MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.086 sec)

MariaDB [blog_db]> SELECT * FROM users;
Empty set (2.102 sec)

MariaDB [blog_db]> DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(50)  | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
4 rows in set (0.346 sec)

C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(50)  | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
4 rows in set (0.127 sec)

MariaDB [blog_db]> INSERT INTO users (email, password, datetime_created)
    -> VALUES ("johnsmith@gmail.com","passwordA", "2021-01-01 01:00:00"),
    -> ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
    -> ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
    -> ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
    -> ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");
Query OK, 5 rows affected (0.296 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+
5 rows in set (0.006 sec)


C:\Users\chris> mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted)
    -> VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
    -> (2, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
    -> (3, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
    -> (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");
Query OK, 4 rows affected (0.276 sec)
Records: 4  Duplicates: 0  Warnings: 0

MariaDB [blog_db]> DESCRIBE posts;
+-----------------+---------------+------+-----+---------+----------------+
| Field           | Type          | Null | Key | Default | Extra          |
+-----------------+---------------+------+-----+---------+----------------+
| id              | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)       | NO   | MUL | NULL    |                |
| title           | varchar(200)  | NO   |     | NULL    |                |
| content         | varchar(5000) | NO   |     | NULL    |                |
| datetime_posted | datetime      | NO   |     | NULL    |                |
+-----------------+---------------+------+-----+---------+----------------+
5 rows in set (0.104 sec)

C:\Users\chris>mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 26
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.022 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> INSERT INTO posts (author_id, title, content, datetime_posted)
    -> VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
    -> (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
    -> (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
    -> (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");
Query OK, 4 rows affected (0.478 sec)
Records: 4  Duplicates: 0  Warnings: 0

MariaDB [blog_db]> DESCRIBE posts
    -> ;
+-----------------+---------------+------+-----+---------+----------------+
| Field           | Type          | Null | Key | Default | Extra          |
+-----------------+---------------+------+-----+---------+----------------+
| id              | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)       | NO   | MUL | NULL    |                |
| title           | varchar(200)  | NO   |     | NULL    |                |
| content         | varchar(5000) | NO   |     | NULL    |                |
| datetime_posted | datetime      | NO   |     | NULL    |                |
+-----------------+---------------+------+-----+---------+----------------+
5 rows in set (0.253 sec)

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_posted     |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  2 |         2 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  3 |         3 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
|  5 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  6 |         1 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  7 |         2 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  8 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+
8 rows in set (0.001 sec)

MariaDB [blog_db]> SELECT * FROM posts WHERE 1
    -> ;
+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_posted     |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  2 |         2 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  3 |         3 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
|  5 |         1 | First Code  | Hello World!          | 2021-01-02 01:00:00 |
|  6 |         1 | Second Code | Hello Earth!          | 2021-01-02 02:00:00 |
|  7 |         2 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  8 |         4 | Fourth Code | Bye bye solar system! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+
8 rows in set (0.001 sec)

MariaDB [blog_db]> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelacruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+
5 rows in set (0.026 sec)

MariaDB [blog_db]> UPDATE posts
    -> SET content = "Hello to the people of the Earth!"
    -> WHERE id = 2;
Query OK, 1 row affected (0.856 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  1 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  2 |         2 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  3 |         3 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
|  5 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  6 |         1 | Second Code | Hello Earth!                      | 2021-01-02 02:00:00 |
|  7 |         2 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  8 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+
8 rows in set (0.001 sec)

MariaDB [blog_db]> UPDATE posts
    -> SET content = "Hello to the people of the Earth!"
    -> WHERE content = "Hello Earth!";
Query OK, 1 row affected (0.103 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  1 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  2 |         2 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  3 |         3 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
|  5 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  6 |         1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  7 |         2 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  8 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+
8 rows in set (0.001 sec)

MariaDB [blog_db]> DELETE FROM users WHERE email = "johndoe@gmail.com";
Query OK, 1 row affected (0.265 sec)

MariaDB [blog_db]> SELECT *  FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.002 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> DESCRIBE users;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int(11)      | NO   | PRI | NULL    | auto_increment |
| email            | varchar(100) | NO   |     | NULL    |                |
| password         | varchar(50)  | NO   |     | NULL    |                |
| datetime_created | datetime     | NO   |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
4 rows in set (0.513 sec)

MariaDB [blog_db]> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelacruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> DESCRIBE posts;
+-----------------+---------------+------+-----+---------+----------------+
| Field           | Type          | Null | Key | Default | Extra          |
+-----------------+---------------+------+-----+---------+----------------+
| id              | int(11)       | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)       | NO   | MUL | NULL    |                |
| title           | varchar(200)  | NO   |     | NULL    |                |
| content         | varchar(5000) | NO   |     | NULL    |                |
| datetime_posted | datetime      | NO   |     | NULL    |                |
+-----------------+---------------+------+-----+---------+----------------+
5 rows in set (0.078 sec)

MariaDB [blog_db]> SELECT * FROM posts;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  1 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  2 |         2 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  3 |         3 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
|  5 |         1 | First Code  | Hello World!                      | 2021-01-02 01:00:00 |
|  6 |         1 | Second Code | Hello to the people of the Earth! | 2021-01-02 02:00:00 |
|  7 |         2 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  8 |         4 | Fourth Code | Bye bye solar system!             | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+
8 rows in set (0.001 sec)

MariaDB [blog_db]>
